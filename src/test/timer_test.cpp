#include <gtest/gtest.h>
#include <timer.h>          /* class to test */
#include <boost/timer.hpp>

TEST(SuiteTestTimer, testNormal)
{
  Timer timer;
  boost::timer t;// start timing
  EXPECT_FALSE(timer.getResult());//False, initial value
  timer.time(2);//thread count to 2
  EXPECT_FALSE(timer.getResult());//False, it hasn't have time to sleep 2 seconds
  sleep(3);//main sleep 3
  EXPECT_TRUE(timer.getResult());//True, main has slept more than 2 seconds
  EXPECT_TRUE(timer.getResult());//True, result is not modified after reading it
  EXPECT_TRUE(t.elapsed()<3.2);//Check that it doesn't take to much time
  timer.join();
}

TEST(SuiteTestTimer, testInterrupt)
{
  Timer timer;
  boost::timer t; // start timing
  EXPECT_FALSE(timer.getResult());//False, initial value
  timer.time(1000);//thread count to 1000
  EXPECT_FALSE(timer.getResult());//False, it hasn't have time to sleep 1000 seconds
  sleep(1);//main sleep 1
  EXPECT_FALSE(timer.getResult());//False, it hasn't have time to sleep enough time, ~999 seconds remaining
  timer.time(3);//thread count to 3, interrupt actual timing
  EXPECT_FALSE(timer.getResult());//False, counting has been interrupted
  sleep(1);//main sleep 1
  EXPECT_FALSE(timer.getResult());//False, it hasn't have time to sleep 3 seconds
  sleep(3);//main sleep 2
  EXPECT_TRUE(timer.getResult());//True, main has slept more than 2 seconds
  EXPECT_TRUE(t.elapsed()<5.2);//Check that thread is interrupted and doesn't count to 1000
  timer.join();
}


int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
