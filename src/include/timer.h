#ifndef TIMER_ACTION_MONITOR
#define TIMER_ACTION_MONITOR

#include <boost/thread.hpp>
#include <boost/chrono.hpp>
//#include <boost/atomic.hpp>

#include <iostream>


class Timer 
{
private:
  boost::thread  m_Thread;
  boost::condition_variable cond;
  boost::mutex mut;

  int secs;
  bool res;
  bool sleeping;
  bool end;

public:
  //! Constructor.
  Timer();
  ~Timer();

  void time(int seconds);

  bool getResult();

  void join();

private:
  void threadFunc();

  void stop();

};

#endif
