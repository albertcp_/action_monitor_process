#include "../include/action_monitor_process.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, ros::this_node::getName());
  ActionMonitor action_monitor_process;
  action_monitor_process.setUp();

  try
  {
    action_monitor_process.start();
  } catch(std::exception& exception)
  {
    action_monitor_process.notifyError(action_monitor_process.SafeguardRecoverableError,0,"ownStart()",exception.what());
    action_monitor_process.stop();    // The process will have to be started by a service call from another process
  }

  ros::Rate loop_rate(10);
  while(ros::ok())
  {
    action_monitor_process.run();
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}
